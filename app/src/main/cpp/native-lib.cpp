#include <jni.h>
#include <string>
#include "firebase/analytics.h"
#include "firebase/analytics/event_names.h"
#include "firebase/analytics/parameter_names.h"
#include "firebase/analytics/user_property_names.h"
#include "firebase/app.h"

extern "C" JNIEXPORT jstring

Java_com_example_jnitest1_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject activity /* this */) {
    std::string hello = "Hello from C++";
    namespace analytics = ::firebase::analytics;
    ::firebase::App *app;
    app = ::firebase::App::Create(::firebase::AppOptions(), env, activity);

    ::firebase::analytics::Initialize(*app);

    analytics::LogEvent(analytics::kEventPostScore, analytics::kParameterScore, 42);

    return env->NewStringUTF(hello.c_str());
}


